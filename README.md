# OpenSource LAMP hardware

## Project Background

Welcome!

In this project we are addressing the need for open source, affordable and easily replicable instrumentation for large-scale diagnosis of infectious diseases.

From our experience, the most promising technique for near point-of-care testing is Reverse Transcription Loop Mediated Amplification (RT-LAMP). It have the robustness typical from nucleic acid testing without the need of the complex and expensive thermocycler. To carry out the test, the reaction need to be incubated at one single temperature (it is isothermal) and be measured through a color change or fluorescence. A low-cost device that performs both functions for multiple samples would greatly increase testing throughput.

We are working in three different prototypes for distinct applications of the technology:

- A device that exploits open source electronics for control of simple PTC heating elements and heat transfer via a recirculating air stream. This removes the need for milled metal components, and greatly reduces device costs. All the work on this device is documented at the Jim Haseloff [Hackster.io](https://www.hackster.io/jim-haseloff) page.





<div id="container" align="center">
<img src="https://hackster.imgix.net/uploads/attachments/1175416/tallboy2_DKSsWWgLPl.png?auto=compress%2Cformat&w=740&h=555&fit=max" width="50%" height="300" align="left">
<img src="https://hackster.imgix.net/uploads/attachments/1166624/_iKy6XJcPB8.blob?auto=compress%2Cformat&w=900&h=675&fit=min" width="50%" height="300px" align="right">
</div>









- An affordable (~5€) opensource waterbath with an integrated transilluminator for heating and reading LAMP results at the same time. The device is documented in the "/WaterBath" folder on this repository. 

<div align="center">
<img src="Photos/waterbath.jpg" width="80%" align="center">
</div>




- A real time opensource LAMP machine, able to incubate the reactions trough an affordable 3D printed metal piece (originally designed by Shingo  Hisakawa from NinjaPCR) and read the fluorescence output in real time. The estimated production cost is ~50€. The device is documented in the "/qByte" folder. 



<div align="center">
    <img src="Photos/new_qlamp.png" width="50%" align="center">
</div>

## Project Team

- Francisco Javier Quero (1,2).
- Prof. Ariel Lindner (2)
- Urs Gaudenz (3)
- Dr. Jenny Molloy (4)
- Prof. Jim Haseloff (5)
- Prof. Chunyan Tang (6)
- Ji Li (1).
- Smitha Hegde (2).

1. OpenFIESTA faculty, Tsinghua University.
2. Centre de Recherches Interdisciplinaires (CRI), University de Paris.
3. Gaudilabs, Switzerland.
4. Department of Plant Sciences, University of Cambridge.
5. Department of Chemical Engineering and Biotechnology, University of Cambridge.
6. Department of Life Science & Health, Tsinghua University.